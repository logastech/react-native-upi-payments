/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TextInput,
  Button,
  TouchableOpacity,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import Modal, {
	ModalTitle,
	ModalContent,
	ModalFooter,
	ModalButton,
	SlideAnimation,
	ScaleAnimation,
} from 'react-native-modals';

import RNUpiPayment from 'react-native-upi-payment';

class App extends Component {

	state = {
		payName: "Dinesh",
		payAmt: "1",
		payUpi: "spicydinu@axisbank",
		payDesc: "Order Payment",
		transId: "Test123",
		processModal: false,
		payStatus: "Pending",
		payTransId : '',
		payMsg : '',
		payData : ''
	}

	payNow = () => {
		const transId = 'test' + (new Date()).getTime();

		RNUpiPayment.initializePayment({
			vpa: this.state.payUpi,
			payeeName: this.state.payName,
			amount: this.state.payAmt,
			transactionRef: transId,
		}, this.payStatus, this.payStatus);
		
		this.setState({transId});
		setTimeout(this.openStatusDialog, 50);
	}

	payStatus = (_data) => {
		console.log(_data);
		const _frompay		=	typeof _data.Status != 'undefined';
		const _status		=	_data.status || _data.Status;
		const _statusTxt	=	_status.slice(0,1).toUpperCase() + _status.slice(1, _status.length).toLowerCase();
		let _message		=	_frompay ?'Invalid Data' :_data.message;
		const _trans_id		=	_data.txnId || "-";

		if(_frompay){
			switch(_data.responseCode){
				case "ZD": _message = "Cancelled by User"; break;
				case "ZM": _message = "Transaction Failed"; break;
				case "UP00": _message = "Transaction Successfull"; break;
				default: _message = "Error in transaction"; break;
			}
		}

		this.setState({
			payStatus : _statusTxt,
			payMsg : _message,
			payTransId : _trans_id,
			payData : JSON.stringify(_data),
		});
	}

	openStatusDialog = () => this.setState({processModal: true});
	closeStatusDialog = () => this.setState({processModal: false});

	render(){
		return (
			<>
			<StatusBar barStyle="default" backgroundColor="#fd7e14" />
			<SafeAreaView style={{flex:1}}>
				<ScrollView contentInsetAdjustmentBehavior="automatic" style={styles.scrollView}>
					<Header />

					<View style={styles.body}>
						<View style={styles.sectionContainer}>
							<Text style={styles.sectionTitle}>Payer Name</Text>
							<TextInput
								placeholder="Enter the name of payer"
								// onChangeText={text => setText(text)}
								defaultValue={this.state.payName}
								style={styles.textBox}
							/>
						</View>
						<View style={styles.sectionContainer}>
							<Text style={styles.sectionTitle}>Payer Name</Text>
							<TextInput
								placeholder="Enter the UPI ID of payer"
								// onChangeText={text => setText(text)}
								defaultValue={this.state.payUpi}
								style={styles.textBox}
							/>
						</View>
						<View style={styles.sectionContainer}>
							<Text style={styles.sectionTitle}>Amount</Text>
							<TextInput
								placeholder="Enter the amount to pay"
								// onChangeText={text => setText(text)}
								defaultValue={this.state.payAmt}
								style={styles.textBox}
							/>
						</View>
						<View style={styles.sectionContainer}>
							<Text style={styles.sectionTitle}>Description</Text>
							<TextInput
								placeholder="Enter the payment description"
								// onChangeText={text => setText(text)}
								defaultValue={this.state.payDesc}
								style={styles.textBox}
							/>
						</View>
						<View style={styles.sectionContainer}>
						</View>
					</View>
				</ScrollView>

				<View style = {styles.btnContainer}>
					<TouchableOpacity onPress={this.payNow}>
						<Text style={styles.textBox, styles.btn}>Pay Now</Text>
					</TouchableOpacity>
				</View>

				<Modal.BottomModal
					visible={this.state.processModal}
					height={0.9}
					width={1}
					useNativeDriver={true}
					onTouchOutside={this.closeStatusDialog}
					onSwipeOut={this.closeStatusDialog}
					modalTitle={
						<ModalTitle title="Payment Status" hasTitleBar />
					}
					footer={
						<ModalFooter>
							<ModalButton text="Close" onPress={this.closeStatusDialog} />
						</ModalFooter>
					}
				>
					<ModalContent style={{flex: 1, backgroundColor: 'fff'}}>
						<ScrollView contentInsetAdjustmentBehavior="automatic" style={styles.scrollView}>
							<View style={styles.body}>
								<View style={styles.sectionContainer}>
									<Text style={styles.sectionTitle}>Status</Text>
									<Text style={styles.sectionDescription}>{this.state.payStatus}</Text>
								</View>
								<View style={styles.sectionContainer}>
									<Text style={styles.sectionTitle}>Transaction ID</Text>
									<Text style={styles.sectionDescription}>{this.state.transId}</Text>
								</View>
								<View style={styles.sectionContainer}>
									<Text style={styles.sectionTitle}>Response ID</Text>
									<Text style={styles.sectionDescription}>{this.state.payTransId}</Text>
								</View>
								<View style={styles.sectionContainer}>
									<Text style={styles.sectionTitle}>Message</Text>
									<Text style={styles.sectionDescription}>{this.state.payMsg}</Text>
								</View>
								<View style={styles.sectionContainer}>
									<Text style={styles.sectionTitle}>Data</Text>
									<Text style={styles.sectionDescription}>{this.state.payData}</Text>
								</View>
							</View>
						</ScrollView>
					</ModalContent>
				</Modal.BottomModal>
			</SafeAreaView>
			</>
		);
	}
};

const styles = StyleSheet.create({
	scrollView: {
		backgroundColor: '#fff',
	},
	engine: {
		position: 'absolute',
		right: 0,
	},
	body: {
		backgroundColor: Colors.white,
		paddingBottom: 20,
	},
	sectionContainer: {
		marginTop: 25,
		paddingHorizontal: 24,
	},
	sectionTitle: {
		fontSize: 24,
		fontWeight: '600',
		color: Colors.black,
		marginBottom: 15,
	},
	sectionDescription: {
		marginTop: 8,
		fontSize: 18,
		fontWeight: '400',
		color: Colors.dark,
	},
	textLabel: {
		width: '100%',
		fontSize: 16,
		padding: 10,
	},
	textBox: {
		fontSize: 18,
		borderColor: '#ccc',
		borderWidth: 1,
		width: '100%',
		paddingVertical: 8,
		paddingHorizontal: 10,
		borderRadius: 5,
		shadowColor: "#666",
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,
		elevation: 2,
	},
	btn: {
		padding: 16,
		color: '#fff',
		alignItems: 'center',
		textAlign: 'center',
		fontSize: 18,
		fontWeight: 'bold',
		textTransform: 'uppercase',
		backgroundColor : "#fd7e14",
	},
	highlight: {
		fontWeight: '700',
	},
	footer: {
		color: Colors.dark,
		fontSize: 12,
		fontWeight: '600',
		padding: 4,
		paddingRight: 12,
		textAlign: 'right',
	},
});

export default App;
